trigger NewInquiryAlert on Inquiry__c (after insert) {
    for (Inquiry__c inquiry : Trigger.new) {
        Listing__c listing = [SELECT Agent__c FROM Listing__c WHERE Id = :inquiry.Listing__c LIMIT 1];
        User listingAgent = [SELECT Email FROM User WHERE Id = :listing.Agent__c LIMIT 1];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        EmailTemplate alertTemplate = [SELECT Id FROM EmailTemplate WHERE Name='New Inquiry Alert' LIMIT 1];
        mail.setTemplateId(alertTemplate.Id);
        mail.setWhatId(inquiry.Id);
        mail.setTargetObjectId(listingAgent.Id);
        mail.setSaveAsActivity(false);
        Messaging.SendEmail(new Messaging.SingleEmailMessage[] {mail});
    }
}