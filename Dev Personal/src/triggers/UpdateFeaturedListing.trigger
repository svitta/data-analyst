trigger UpdateFeaturedListing on Listing__c (before insert, before update) {
	for (Listing__c listing : Trigger.new) {
		if (listing.Featured_Listing__c == true) {
			for (Listing__c featuredListing : [SELECT Id, Featured_Listing__c FROM Listing__c 
				WHERE Featured_Listing__c = true AND Id != :listing.Id]) {
				featuredListing.Featured_Listing__c = false;
				Update featuredListing;		
			}	
		}
	}
}