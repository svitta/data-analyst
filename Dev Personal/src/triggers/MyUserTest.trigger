trigger MyUserTest on User (before Update,After Update) {
    
    
    if(Trigger.isUpdate)
    {
       system.debug('*************isUpdate*******************');
        Map<Id,User> mapUser = new Map<Id, User>();
    
        if(Trigger.isBefore)
        {
        system.debug('*************isBefore*******************'); 
            for(User u : Trigger.new)
            {
                if(!u.IsActive && u.ManagerId!=null)
                {
                    
                    mapUser.put(u.Id,u);
                }
                
            }   
            
            system.debug('mapUser======' +  mapUser);
            
        }
        else if(Trigger.isAfter)
        {
                   system.debug('*************isAfter*******************');
            Map<Id,Id> mgrId = new Map<Id,Id>();
            For(User u1: Trigger.new)
            {
                if(!u1.isActive && u1.managerId !=null)
                {
                mgrId.put(u1.id, u1.ManagerId);
                }
            }
            system.debug('mgrId===' + mgrId);
            
            
            if(!mgrId.isEmpty())
            {
               
                Contact tempContact;
             //   Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            User recepient = [select id,managerId,email, firstName, lastName from user where id in : mgrId.keySet()];
                 system.debug(recepient);
               
            EmailTemplate et = [SELECT Id, Subject, HtmlValue, Body FROM EmailTemplate WHERE id = '00XA0000001Djhk'];
                
            
                String subject = et.Subject;
subject = subject.replace('{!Target_User.Id}', recepient.id);
subject = subject.replace('{!Target_User.FirstName}', recepient.firstName);
                subject = subject.replace('{!Target_User.LastName}', recepient.LastName);


String Body = et.HtmlValue;
Body = Body.replace('{!Target_User.Id}', recepient.id);
Body = Body.replace('{!Target_User.FirstName}', recepient.firstName);
Body = Body.replace('{!Target_User.LastName}', recepient.LastName);
                
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
email.setTargetObjectId(recepient.managerId);
email.setSubject(subject);
email.setHtmlBody(Body);
         email.setSaveAsActivity(false);         
Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
                
           
            }
        }
        
    }
}