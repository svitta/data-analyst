public with sharing class TestSupport {
	
	public static User getAgent() {
		User agent = [Select Id From User Where Profile.Name = 'System Administrator' Limit 1];
		if ([Select Count() From Agent_On_Website__c Where User__c != :agent.Id] == 0) {
			insert new Agent_On_Website__c(User__c = agent.Id);
		}
		return agent;
	}
	
	public static Listing__c createListing(Boolean isFeatured, Id agentId) {
		Listing__c l = new Listing__c();
		l.Address__c = '123 Main Street';
		l.Age__c = 30.0;
		l.Agent__c = agentId;
		l.Asking_Price__c = 500000.00;
		l.Bathrooms__c = 2.0;
		l.Bedrooms__c = 4.0;
		l.Description__c = 'This is the description of the property.';
		l.Featured_Listing__c = isFeatured;
		l.Master_Picture_ID__c = '1';
		l.Neighborhood__c = 'South Central';
		l.Size__c = 37000.0;
		l.Status__c = 'Listed';
		l.Tag_Line__c = 'Perfect Fixer Upper';
		insert l;
		return l;
	}	
}