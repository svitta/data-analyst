@isTest
private with sharing class TestClass {

	testmethod static void testTriggers() {
		// Make sure we have a listing, to do that we need a valid user
		User agent = testSupport.getAgent();
		Contact newContact = new Contact(LastName='Jones', FirstName='John');
		insert newContact;
		Listing__c l = TestSupport.createListing(true, agent.Id);
		Inquiry__c inq = new Inquiry__c();
		inq.Listing__c = l.Id;
		inq.Full_Name__c = 'Joe Smith';
		inq.Contact__c = newContact.Id;
		inq.Inquiry_Text__c = 'Can I have it for free?';
		inq.Phone__c = '415.901.8599';
		insert inq;
		Listing__c fl = TestSupport.createListing(true, agent.Id);
	}

}