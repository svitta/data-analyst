global class PublicWebService {

	// Soap Web Service
	webservice static List<Listing__c> getListings(
				Integer bedrooms, 
				Integer bathrooms, 
				String neighborhood, 
				Integer maxPrice) {

		ListingsController lc = new ListingsController();
		lc.minBedrooms = bedrooms;
		lc.minBathrooms = bathrooms;
		lc.neighborhood = neighborhood;
		lc.maxPrice = maxPrice;
		return lc.listings;
	}
	
	public String getListingFeed() {
		return PublicWebService.restListing();
	}
		
	// Rest Atom Feed Service
	public static String restListing() {
		String bedrooms = Apexpages.currentPage().getParameters().get('bedrooms');	
		String bathrooms = Apexpages.currentPage().getParameters().get('bathrooms');	
		String neighborhood = Apexpages.currentPage().getParameters().get('neighborhood');	
		String max_price = Apexpages.currentPage().getParameters().get('max_price');	
		List<Listing__c> l = getListings(
			(bedrooms == null ? -1 : Integer.valueOf(bedrooms)), 
			(bathrooms == null ? -1 : Integer.valueOf(bathrooms)),
			(neighborhood == null ? 'All' : neighborhood),
			(max_price == null ? -1 : Integer.valueOf(max_price))
		);
		String urlBase = 'http://' + Site.getDomain();
		return listingFeed(l, urlBase);
		
	}
	
	private static String listingFeed(List<Listing__c> l, String urlBase) {
		String output = '<feed xmlns="http://www.w3.org/2005/Atom">';
		output += '<author><name>Dave Carroll</name></author>';
		output += '<title>Public Listings</title>';
		output += '<updated>' + datetime.now().formatGmt('yyyy-MM-dd HH:mm:ss').replace(' ', 'T') + 'z</updated>';
		for (Listing__c ll : l) {
			output += listingFeedEntry(ll, urlBase);
		}
		output += '</feed>';
		System.debug('\n\nFeed: \n' + output);
		return output;
	}
	
	private static String listingFeedEntry(Listing__c l, String urlBase) {
		
		String output = '<entry>';	
		output += '<title>' + l.address__c + '</title>';	
		output += '<author>';
		output += '<name>' + l.CreatedById + '</name>';
		output += '</author>';
		output += '<Tag_Line__c>' + l.Tag_Line__c + '</Tag_Line__c>';
		output += '<updated>' + l.SystemModstamp.formatGmt('yyyy-MM-dd HH:mm:ss').replace(' ', 'T') + 'z</updated>';
		
		output += '<SystemModstamp>' + l.SystemModStamp + '</SystemModstamp>'; 
		 //, Status__c, Size__c, OwnerId, Neighborhood__c, Name, LastModifiedDate, LastModifiedById, IsDeleted, Id, Featured_Listing__c, Description__c, CreatedDate, CreatedById, Bedrooms__c, Bathrooms__c, Asking_Price__c, Agent__c, Age__c, Address__c From Listing__c';
		output += '<content type="html">' + l.Description__c + '</content>';
		output += '<link href="' + urlBase + '/listing?id=' + l.Id + '" />';
		output += '<bedrooms>' + l.Bedrooms__c + '</bedrooms>';
		output += '</entry>';
		return output;
	}	

	testmethod static void test() {
		PublicWebService.getListings(4, 2, 'South Central', 10000000);
		PublicWebService pws = new PublicWebService();
		pws.getListingFeed();
		Test.setCurrentPage(new PageReference('/feeds?bedrooms=4&bathrooms=2&neighborhood=South Central&max_price=1000000'));
		pws.getListingFeed();
	}
			
}