public with sharing class SampleData {

    
    testmethod static void test() {
        SampleData.addSampleData();
    }
    public static void addSampleData() {
        
        
        List<NewsItem__c> newsItems = new List<NewsItem__c>();
        newsitems.add(new NewsItem__c(
            Name = 'Big Cloud Realty Uses Force.com Sites to Run it\'s New Web Site', 
            DateText__c = 'March 18, 2009', 
            Body__c = 'Big Cloud Realty has implemented Force.com and is using the Sites feature to deliver it\'s new and improved web site.', 
            Active__c = true));
        newsitems.add(new NewsItem__c(
            Name = 'It\'s a Great Time to Invest in a New Home', 
            DateText__c = 'March 1, 2009', 
            Body__c = 'It\'s a great time to buy now because interest rates are at 10 year lows. Also prices have been coming down lately so now is a good time to get in on the bottom.', 
            Active__c = true));
        newsitems.add(new NewsItem__c(
            Name = 'Commission Reduction Promotion Running During March 2009', 
            DateText__c = 'March 12, 2009', 
            Body__c = 'We\'re having a promotion this week - if you list your house with us within March, 2009 we\'ll knock 0.5% off of our commission. Hurry up before it\'s too late!', 
            Active__c = true));
        newsitems.add(new NewsItem__c(
            Name = 'Jennifer Taylor Welcomed as Newest Agent to the Team', 
            DateText__c = 'March 10, 2009', 
            Body__c = 'Please welcome the newest real estate agent to our team: Jennifer Taylor. Jennifer comes with 10 years experience selling homes in the Bay Area. We\'re looking forward to putting her to work for you!', 
            Active__c = true));
        newsitems.add(new NewsItem__c(
            Name = 'Selling Home May Be Influenced by What Buyers Can\'t See', 
            DateText__c = 'June 22, 2009', 
            Body__c = 'It\'s not always what buyers can see in a home that causes them to want to buy it or not. Sometimes it\'s the way the home feels. I\'m not talking about staging, the size, or how spacious the home is, although those factors are important too. In this column I\'m focusing on how buyers allergies may be affected when they tour your home.', 
            Active__c = true));
        newsitems.add(new NewsItem__c(
            Name = 'Big Cloud is Innovation', 
            DateText__c = 'June, 22, 2009', 
            Body__c = 'The Big Cloud Realty site was chosen as an exemplary site built on the Force.com Platform using the newly released Force.com Sites feature.', 
            Active__c = true));
        
        List<Listing__c> listings = new List<Listing__c>();
        listings.add(new Listing__c(Tag_Line__c = 'Luxury home in San Jose', 
        Status__c = 'Listed', 
        Size__c = 2531, 
        Neighborhood__c = 'San Jose', 
        Master_Picture_ID__c = '00P80000003X63w', 
        Featured_Listing__c = false, 
        Description__c = '1 Master Bedroom Suite, 1 Shower over Tub, 1 Stall Shower, 2 or More Tubs, 220 Volts in Laundry Area, Cable TV Available, Ceilings Insulated, Double Pane Windows, Formal Entry, Gas Hookup in Kitchen, Gas Hookup in Laundry Area, Gas Water Heater, Ground Floor Bedroom, High Ceilings, Insulation, Laundry Area Inside, Separate Dining Room, Tub in Master Bedroom, Vaulted/Cathedral Ceiling, Wall to Wall Carpeting, Walls Insulated.', 
        Bedrooms__c = 4, 
        Bathrooms__c = 4, 
        Asking_Price__c = 1300000, 
        Agent__c = TestSupport.getAgent().Id, 
        Age__c = 4, 
        Address__c = '1368 Brecon Court')); 
        listings.add(new Listing__c(Tag_Line__c = 'Nice big house for a big family', 
        Status__c = 'Listed', 
        Size__c = 2989,  
        Neighborhood__c = 'Cupertino', 
        Master_Picture_ID__c = '00P80000003GF9R', 
        Map__c = 'http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=4146+Marston+Ln,+santa+clara,+ca&sll=37.0625,-95.677068&sspn=40.953203,77.167969&ie=UTF8&ll=37.397113,-121.948586&spn=0.010058,0.01884&z=16&iwloc=addr', 
        Featured_Listing__c = false, 
        Description__c = '1 Master Bedroom Suite, 1 Tub, 2 or More Stall Showers, Area Carpeting, Attic, Attic Fan, Built-In Vacuum, Cable TV Available, Ceiling Fan(s), Double Pane Windows, Formal Entry, Gas Water Heater, Ground Floor Bedroom, High Ceilings, Laundry Area Inside, Loft, Pantry, Recreation Room, Separate Dining Room, Tub in Master Bedroom, Tub with Jets, Wall to Wall Carpeting.', 
        Bedrooms__c = 4, 
        Bathrooms__c = 4, 
        Asking_Price__c = 1100000, 
        Agent__c = TestSupport.getAgent().Id, 
        Age__c = 6, 
        Address__c = '4146 Marston Ln')); 
        listings.add(new Listing__c(Tag_Line__c = 'Beautiful new house in Santa Clara', 
        Status__c = 'Listed', 
        Size__c = 2989, 
        Neighborhood__c = 'Santa Clara', 
        Master_Picture_ID__c = '00P80000003GFAK', 
        Map__c = 'http://maps.google.com/maps?f=q&source=s_q&hl=en&q=819+E+River+Pkwy,+Santa+Clara,+Santa+Clara,+California+95054&sll=37.397113,-121.948586&sspn=0.010058,0.01884&ie=UTF8&cd=1&geocode=FQqoOgIdnjW7-A&split=0&ll=37.398545,-121.948779&spn=0.010058,0.01884&z=16&', 
        Featured_Listing__c = true, 
        Description__c = '1 Master Bedroom Suite, 2 or More Stall Showers, 2 or More Tubs, 220 Volts in Kitchen, Area Carpeting, Dining Area in Family Room, Double Pane Windows, Electric Water Heater, Fire Sprinkler System, Garden House Windows, Gas Water Heater, Ground Floor Bedroom, High Ceilings, Laundry Area Inside, Library, Pantry, Vaulted/Cathedral Ceiling, Wall to Wall Carpeting.', 
        Bedrooms__c = 4, 
        Bathrooms__c = 4, 
        Asking_Price__c = 1249000, 
        Agent__c = TestSupport.getAgent().Id, 
        Age__c = 5, 
        Address__c = '819 E River Pkwy')); 
        listings.add(new Listing__c(Tag_Line__c = 'Brand new home by high end designer', 
        Status__c = 'Listed', 
        Size__c = 1750, 
        Neighborhood__c = 'Santa Clara', 
        Master_Picture_ID__c = '00P80000003GFAt', 
        Map__c = 'http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=731+Camino+Dr++Santa+Clara,+CA+95050&sll=37.398545,-121.948779&sspn=0.010058,0.01884&ie=UTF8&z=16&iwloc=addr', 
        Featured_Listing__c = false, 
        Description__c = '2 or More Master Suites, 2 or More Showers over Tubs, 2 or More Stall Showers, 2 or More Tubs, 220 Volts in Laundry Area, Cable TV Available, Ceiling Fan(s), Ceilings Insulated, Double Pane Windows, Formal Entry, Garden House Windows, Gas Hookup in Kitchen, Gas Water Heater, Ground Floor Bedroom, High Ceilings, Insulation, Laundry Area in garage, Separate Dining Room, Separate Living Unit, Skylight(s), Tub in Master Bedroom, Tub with Jets, Wall to Wall Carpeting, Walls Insulated.', 
        Bedrooms__c = 3, 
        Bathrooms__c = 3, 
        Asking_Price__c = 898000, 
        Agent__c = TestSupport.getAgent().Id, 
        Age__c = 1, 
        Address__c = '731 Camino Dr')); 
        listings.add(new Listing__c(Tag_Line__c = 'Gorgeous modern bungalow', 
        Status__c = 'Listed', 
        Size__c = 3041, 
        Neighborhood__c = 'San Jose', 
        Master_Picture_ID__c = '00P80000003GFAz', 
        Featured_Listing__c = false, 
        Description__c = '1 Master Bedroom Suite, 1 Stall Shower, 2 or More Showers over Tubs, 2 or More Tubs, 220 Volts in Kitchen, 220 Volts in Laundry Area, Bay Windows, Bonus Room, Cable TV Available, Double Pane Windows, Formal Entry, Gas Water Heater, Ground Floor Bedroom, High Ceilings, Laundry Area Inside, Satellite Dish, Separate Dining Room, Skylight(s), Tub in Master Bedroom, Tub with Jets, Utility Room, Vaulted/Cathedral Ceiling, Wall to Wall Carpeting.', 
        Bedrooms__c = 4, 
        Bathrooms__c = 3, 
        Asking_Price__c = 1000000, 
        Agent__c = TestSupport.getAgent().Id, 
        Age__c = 7, 
        Address__c = '3933 Carracci Lane')); 
        insert listings;
    }
}