public with sharing class mulController {
public Double value1{get;set;}
public Double value2{get;set;}
public Double result{get;set;}
public void mul(){
result=value1*value2;
}
public void reset(){
value1=null;
value2=null;
result=null;
}
}