public class NewsWidgetController {
	
	public List<NewsItem__c> newsItems {
		get {
			return [SELECT Name, DateText__c FROM NewsItem__c WHERE Active__c = true ORDER BY CreatedDate DESC];
		}
	}
	
	testmethod static void test() {
		NewsWidgetController nwc = new NewsWidgetController();
		List<NewsItem__c> news = nwc.newsItems;
	}
}