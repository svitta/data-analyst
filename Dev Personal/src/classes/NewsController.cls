public class NewsController {
	
	public List<NewsItem__c> newsItems {
		get {
			return [SELECT Name, DateText__c, Body__c, CreatedDate FROM NewsItem__c ORDER BY CreatedDate DESC];
		}
	}
	
	testmethod static void test() {
		NewsController nc = new NewsController();
		List<NewsItem__c> news = nc.newsItems;
	}
}