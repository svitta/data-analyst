public class LoginController {

	public String username { get; set; }
	
	public String password { get; set; }
	
	public PageReference login() {
		return Site.login(username, password, '/home');
	}
	
	testmethod static void test() {
		LoginController lc = new LoginController();
		lc.username = 'test';
		lc.password = 'test';
		PageReference p = lc.login();
	}
}