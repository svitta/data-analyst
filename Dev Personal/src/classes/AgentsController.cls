public class AgentsController {
	
	public List<User> agents {
		get {
			List<User> gents = new List<User>();
			for (Agent_On_Website__c aow : [SELECT User__r.Id, User__r.Name FROM Agent_On_Website__c ]) {
				gents.add(aow.User__r);	
			}
			return gents;
		}
	}
 
	testmethod static void test() {
		AgentSController ac = new AgentSController();
		User agent = TestSupport.getAgent();
		insert new Agent_On_Website__c(User__c = agent.Id);
		List<User> agents = ac.agents;
	}


}