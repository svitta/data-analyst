public class HomeController {
	
	public Listing__c featured {
		get { 
			List<Listing__c> listing = [SELECT Name, Tag_Line__c, Bedrooms__c, Bathrooms__c, Size__c, Description__c,
				Asking_Price__c, Master_Picture_ID__c FROM Listing__c WHERE Featured_Listing__c = true LIMIT 1];
			if (listing.size() == 0) {
				return null;
			} else {
				return listing[0];
			}
		}
	}
	
	testmethod static void test() { 
		TestSupport.createListing(true, TestSupport.getAgent().Id);
		TestSupport.createListing(true, TestSupport.getAgent().Id);
		TestSupport.createListing(true, TestSupport.getAgent().Id);
		HomeController hc = new HomeController();
		Listing__c listing = hc.featured;
	}
}