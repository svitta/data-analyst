public class ListingsController {

    public Integer minBedrooms { get; set; }
    public Integer minBathrooms { get; set; }
    public String neighborhood { get; set; }
    public Integer maxPrice { get; set; }
    
    private final String baseSOQL = 
    	'Select Master_Picture_Id__c, Tag_Line__c, Address__c, ' +
    			'Size__c, Neighborhood__c, Id, Description__c, ' +
    			'Bedrooms__c, Bathrooms__c, Asking_Price__c, ' + 
    			'CreatedById, SystemModstamp From Listing__c';
   
	public ListingsController() {
    	// set defaults
    	maxPrice = 1100000;
    	minBedrooms = -1;
    	minBathrooms = -1;
    	neighborhood = 'All';
    }
    
    public Listing__c[] listings {
        get {
       		String soql = baseSOQL + ' WHERE Status__c = \'Listed\''; 
 			
 			if (minBedrooms != -1) {
 				soql += ' AND Bedrooms__c >= ' + String.valueOf(minBedrooms);
 			}
 			if (minBathrooms != -1) {
 				soql += 'AND Bathrooms__c >= ' + String.valueOf(minBathrooms);
 			}
 			if (neighborhood != 'All') {
 				soql += 'AND Neighborhood__c = \'' + neighborhood + '\'';
        	}
            return Database.query(soql);
        }
    }
    
}