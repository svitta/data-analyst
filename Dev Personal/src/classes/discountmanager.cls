public with sharing class discountmanager{
public vitta__Mycustomobj__c userobj{get;set;}
public discountmanager(apexpages.standardcontroller discountmanagerobj){
userobj=(vitta__Mycustomobj__c)discountmanagerobj.getrecord();
}
public PageReference recordupdate()
{
if(userobj.vitta__Manager__c=true)
userobj.vitta__Discount__c=40;
else
userobj.vitta__Discount__c=20;
update userobj;
PageReference pageRef=new pageReference('/'+userObj.Id);
return pageref;
}
}