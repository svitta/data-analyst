
// Test Git 
// this is second commit

public class AgentController {
	
	public User agent {
		get {
			String id = ApexPages.currentPage().getParameters().get('id');
			return [SELECT Name, Title, Phone, Fax FROM User WHERE Id=:id LIMIT 1]; 
		}
	}
	
	testmethod static void test() {
		AgentController ac = new AgentController();
		User agent = TestSupport.getAgent(); 
		Test.setCurrentPage(new PageReference('/agent?id=' + agent.Id));
		System.assertEquals(ac.agent.Id, agent.Id, 'Agent Ids do not match.');
	}
}