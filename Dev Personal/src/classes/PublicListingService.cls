public class PublicListingService {

	public static String selectStatement = 'Select Address__c, Age__c, Agent__c, Asking_Price__c, Bathrooms__c, Bedrooms__c, Map__c, NeighborHood__c, Size__c, Status__c From Listing__c';
	
	public List<Listing__c> getPublicListings(Integer bedrooms, Integer bathrooms, String neighborhood, Long maxPrice) {
		List<Listing__c> vals = Database.query(buildSOQL(bedrooms, bathrooms, neighborhood, maxPrice));
		return vals;
	}

	testmethod static void testService() {
		System.debug('\n\n' + PublicWebService.getListings(4, 2, 'South Central', 1000000));
		PublicListingService pls = new PublicListingService();
		pls.getPublicListings(4, 2, 'South Central', 1000000);
	}
		
	private String buildSOQL(Integer bedrooms, Integer bathrooms, String neighborHood, Long maxPrice) {
		List<String> params = new List<String>();
		if (bedrooms > 0) {
			params.add('Bedrooms__c >= ' + String.valueOf(bedrooms));
		}
		if (bathrooms > 0) {
			params.add('Bathrooms__c >= ' + String.valueOf(bathrooms));
		}
		if (neighborHood != null && neighborHood != '') {
			params.add('Neighborhood__c = \'' + neighborHood + '\'');
		}
		if (maxPrice > 0) {
			params.add('Asking_Price__c <= ' + maxPrice);
		}
		String out = '';
		for (String param : params) {
			out += ' ' + param + ' and ';
		}
		if (params.size() > 0) {
			out = ' where ' + out.subString(0, out.length() - 5);
		}
		out = selectStatement + out;
		return out;
	}
}