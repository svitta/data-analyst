public class WatchListController {

	public WatchItem__c[] watchItems {
		get {
			return [SELECT Listing__c, Listing__r.Address__c FROM WatchItem__c];
		}
	}
	
	testmethod static void test() {
		WatchListController wlc = new WatchListController();
		WatchItem__c[] items = wlc.watchItems;
	}
}