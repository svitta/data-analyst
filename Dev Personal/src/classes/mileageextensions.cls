public class mileageextensions {

private final mileage__c mileageObj;
public mileageextensions(ApexPages.StandardController controller) {
this.mileageObj = (mileage__c)controller.getrecord();
}
public mileage__c[] getTodaysmileageRecords() {
String createdbyId = UserInfo.getUserId();
Mileage__c[] mileageList =
[SELECT name, miles__c
FROM Mileage__c
WHERE Date__c = TODAY
AND createdbyid = :createdbyId];
return mileageList;
    }}