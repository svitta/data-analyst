public class MileageUtil {
 static final integer MAX_MILES_PER_DAY  = 500;
 Mileage__C[] miles ;
  public static void areMilesAllowed (Mileage__c[] miles){
   string createdbyID = userinfo.getUserId();
   double totalMiles = 0;
   for(Mileage__c mq :[SELECT Miles__C from Mileage__c 
                       where Date__c = Today 
                       AND Mileage__c.createdBYID =:createdbyId
                       AND Miles__C != null]){
                    
                       totalMiles += mq.Miles__c;
                       }
   for(Mileage__c m : Miles){
   totalMiles += m.Miles__c;
   If (totalMiles > MAX_MILES_PER_DAY){
   m.addError('Mileage request limit exceeds daily limit:'+ MAX_MILES_PER_DAY);}
   }
  }
  }