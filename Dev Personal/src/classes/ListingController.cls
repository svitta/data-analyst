public class ListingController {
	
	private String listingId {
		get {
			return ApexPages.currentPage().getParameters().get('id');
		}
	}
	
	public Listing__c listing {
		get {
			return [SELECT Id, Name, Address__c, Age__c, Agent__r.Name, Asking_Price__c, Bathrooms__c,
				Bedrooms__c, Description__c, Neighborhood__c, Status__c, Tag_Line__c, Master_Picture_ID__c
				FROM Listing__c WHERE Id=:listingId LIMIT 1]; 
		}
	}
	
	public Attachment[] pictures {
		get {
			return [SELECT Id FROM Attachment WHERE ParentId=:listingId ORDER BY CreatedDate ASC];
		}
	}
	
	public PageReference watch() {
		WatchItem__c watchItem = new WatchItem__c(Listing__c = listingId);
		Insert watchItem;
		return null;
	}
	
	testmethod static void test() {
		User agent = TestSupport.getAgent();
		Listing__c l = TestSupport.createListing(true, agent.Id);
		
		Test.setCurrentPage(new PageReference('/listing?id=' + l.Id));
		ListingController lc = new ListingController();
		
		Listing__c controllerListing = lc.listing; 
		System.assertEquals(lc.pictures.size(), 0, 'Should not have any pictures attached to this listing.');

		lc.watch();		
	}
}