// This class updates the Hello field on account records that are
// passed to it.
   public class MyHelloWorld {
      public static void addHelloWorld(Account[] accs){
        for (Account a:accs){
            if (a.Hello__c != 'World') {
               a.Hello__c = 'World';
            }
        }
      }
      
      //This tests that inserting an account automatically adds the "World" into the Hello__c field
      public static testmethod void HelloWorldtest()
      {
          Account a = new account(name = 'testHelloWorld');
          insert a;
          a = [select id, name, Hello__c from Account where id = :a.id];
          System.assert(a.Hello__c == 'World', 'World was not inserted into Hello__c');
      }
   }