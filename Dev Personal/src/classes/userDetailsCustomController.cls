public with sharing class userDetailsCustomController {
public vitta__Mycustomobj__c userObj{get;set;}
public userDetailsCustomController(ApexPages.StandardController userDetailsObj){
userObj=(vitta__Mycustomobj__c)userDetailsObj.getRecord();
}
public void designationChange(){
if(userObj.Designation__c=='Manager') 
userObj.Discount__c=25;
else if(userObj.Designation__c=='Sales Person')
userObj.Discount__c=12;
else if(userObj.Designation__c=='Accountant')
userObj.Discount__c=15;
else if(userObj.Designation__c=='CEO')
userObj.Discount__c=30;
else
userObj.Discount__c=10;
}
public PageReference updateRecord(){
if(userObj.vitta__Additional_Discount_Allowed__c==true)
userObj.Discount__c=userObj.Discount__c+5;
update userObj;
PageReference pageRef=new pageReference('/'+userObj.Id);
return pageRef;
}
}